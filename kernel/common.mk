#
# USPi - An USB driver for Raspberry Pi written in C
# Copyright (C) 2014-2020  R. Stange <rsta2@o2online.de>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

RASPPI = 1

ifeq ($(strip $(USPIHOME)),)
USPIHOME = ..
endif

AARCH64	?= 0

ifeq ($(strip $(AARCH64)),0)
RASPPI	?= 1
TOOLCHAIN ?= arm-none-eabi-
else
RASPPI	= 3
TOOLCHAIN ?= aarch64-linux-gnu-
endif

CC := $(TOOLCHAIN)gcc
CXX := $(TOOLCHAIN)g++
LD := $(TOOLCHAIN)ld
AS := $(TOOLCHAIN)as
AR := $(TOOLCHAIN)ar
OBJCOPY := $(TOOLCHAIN)objcopy

DEPDIR := .deps
USPI := 1

ifeq ($(strip $(AARCH64)),0)
ifeq ($(strip $(RASPPI)),1)
ARCH	?= -march=armv6j -mtune=arm1176jzf-s -mfpu=vfp -mfloat-abi=softfp
ASARCH  ?= -mcpu=arm1176jzf-s -mfpu=vfp -mfloat-abi=softfp
TARGET	?= kernel
else ifeq ($(strip $(RASPPI)),2)
ARCH	?= -march=armv7-a -mtune=cortex-a7
TARGET	?= kernel7
else
ARCH	?= -march=armv8-a -mtune=cortex-a53
TARGET	?= kernel8-32
endif
else
ARCH	?= -march=armv8-a -mtune=cortex-a53 -mlittle-endian -mcmodel=small -DAARCH64=1
endif

ASFLAGS = --warn $(ASARCH)
CFLAGS = -Wall -O2 -ffreestanding $(ARCH) -fsigned-char -I$(USPIHOME)/kernel -D__RASPBERRY_PI__
CPPFLAGS = $(CFLAGS) -fno-exceptions -fno-unwind-tables -fno-rtti
LDFLAGS = -T $(USPIHOME)/kernel/raspberry.ld -nostartfiles -fno-exceptions -fno-unwind-tables -fno-rtti -Wl,-Map=kernel.map -o kernel.elf

CFLAGS += -DRASPPI=$(RASPPI)

ifeq ($(USPI),1)
CFLAGS += -DHAVE_USPI
endif

-include $(USPIHOME)/kernel/config.mk

LDFLAGS += -L$(USPIHOME)/kernel
LIBS += -lkernel
LIBS_DEP += $(USPIHOME)/kernel/libkernel.a

%.o: %.c
	@mkdir -p $(DEPDIR)/$(@D)
	$(CC) $(CFLAGS) -std=c99 -MD -MP -MF $(DEPDIR)/$*.Tpo -c -o $@ $<
	@mv -f $(DEPDIR)/$*.Tpo $(DEPDIR)/$*.Po

%.o: %.cpp
	@mkdir -p $(DEPDIR)/$(@D)
	$(CXX) $(CPPFLAGS) -MD -MP -MF $(DEPDIR)/$*.Tpo -c -o $@ $<
	@mv -f $(DEPDIR)/$*.Tpo $(DEPDIR)/$*.Po

%.o: %.s
	$(AS) $(ASFLAGS) -o $@ $<

%.o: %.png
	$(LD) -r -b binary -o $@ $<

%.o: %.ogg
	$(LD) -r -b binary -o $@ $<

%.o: %.txt
	$(LD) -r -b binary -o $@ $<

//
// usbgamepad.h
//
// USPi - An USB driver for Raspberry Pi written in C
// Copyright (C) 2014-2018  R. Stange <rsta2@o2online.de>
// Copyright (C) 2014  M. Maccaferri <macca@maccasoft.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef _uspi_usbgamepad_h
#define _uspi_usbgamepad_h

#include <uspi/usbfunction.h>
#include <uspi/usbendpoint.h>
#include <uspi/usbrequest.h>
#include <uspi/usbhid.h>
#include <uspi/types.h>
#include <uspi.h>

typedef enum TGamePadType
{
	GamePadTypeStandard,
	GamePadTypePS3,
	GamePadTypeXbox360,
	GamePadTypeUnknown
} TGamePadType;

// The following enums are valid for known gamepads only!

enum TGamePadButton		// Digital button (bit masks)
{
	GamePadButtonGuide	= BIT(0),
#define GamePadButtonXbox	GamePadButtonGuide
#define GamePadButtonPS		GamePadButtonGuide
#define GamePadButtonHome	GamePadButtonGuide
	GamePadButtonLT		= BIT(3),
#define GamePadButtonL2		GamePadButtonLT
#define GamePadButtonLZ		GamePadButtonLT
	GamePadButtonRT		= BIT(4),
#define GamePadButtonR2		GamePadButtonRT
#define GamePadButtonRZ		GamePadButtonRT
	GamePadButtonLB		= BIT(5),
#define GamePadButtonL1		GamePadButtonLB
#define GamePadButtonL		GamePadButtonLB
	GamePadButtonRB		= BIT(6),
#define GamePadButtonR1		GamePadButtonRB
#define GamePadButtonR		GamePadButtonRB
	GamePadButtonY		= BIT(7),
#define GamePadButtonTriangle	GamePadButtonY
	GamePadButtonB		= BIT(8),
#define GamePadButtonCircle	GamePadButtonB
	GamePadButtonA		= BIT(9),
#define GamePadButtonCross	GamePadButtonA
	GamePadButtonX		= BIT(10),
#define GamePadButtonSquare	GamePadButtonX
	GamePadButtonSelect	= BIT(11),
#define GamePadButtonBack	GamePadButtonSelect
#define GamePadButtonShare	GamePadButtonSelect
#define GamePadButtonCapture	GamePadButtonSelect
	GamePadButtonL3		= BIT(12),		// Left axis button
#define GamePadButtonSL		GamePadButtonL3
	GamePadButtonR3		= BIT(13),		// Right axis button
#define GamePadButtonSR		GamePadButtonR3
	GamePadButtonStart	= BIT(14),		// optional
#define GamePadButtonOptions	GamePadButtonStart
	GamePadButtonUp		= BIT(15),
	GamePadButtonRight	= BIT(16),
	GamePadButtonDown	= BIT(17),
	GamePadButtonLeft	= BIT(18),
	GamePadButtonPlus	= BIT(19),		// optional
	GamePadButtonMinus	= BIT(20),		// optional
	GamePadButtonTouchpad	= BIT(21)		// optional
};

enum TGamePadAxis		// Axis or analog button
{
	GamePadAxisLeftX,
	GamePadAxisLeftY,
	GamePadAxisRightX,
	GamePadAxisRightY,
	GamePadAxisButtonLT,
#define GamePadAxisButtonL2	GamePadAxisButtonLT
	GamePadAxisButtonRT,
#define GamePadAxisButtonR2	GamePadAxisButtonRT
	GamePadAxisButtonUp,
	GamePadAxisButtonRight,
	GamePadAxisButtonDown,
	GamePadAxisButtonLeft,
	GamePadAxisButtonL1,
	GamePadAxisButtonR1,
	GamePadAxisButtonTriangle,
	GamePadAxisButtonCircle,
	GamePadAxisButtonCross,
	GamePadAxisButtonSquare,
	GamePadAxisUnknown
};

typedef enum TGamePadLEDMode
{
	GamePadLEDModeOff,
	GamePadLEDModeOn1,
	GamePadLEDModeOn2,
	GamePadLEDModeOn3,
	GamePadLEDModeOn4,
	GamePadLEDModeOn5,
	GamePadLEDModeOn6,
	GamePadLEDModeOn7,
	GamePadLEDModeOn8,
	GamePadLEDModeOn9,
	GamePadLEDModeOn10,
	GamePadLEDModeUnknown
} TGamePadLEDMode;

enum TGamePadRumbleMode
{
	GamePadRumbleModeOff,
	GamePadRumbleModeLow,
	GamePadRumbleModeHigh,
	GamePadRumbleModeUnknown
};

#define GAMEPAD_AXIS_DEFAULT_MINIMUM	0
#define GAMEPAD_AXIS_DEFAULT_MAXIMUM	255

struct TUSBGamePadDevice;

typedef struct TUSBGamePadDevice
{
	TUSBFunction m_USBFunction;
	unsigned m_nDeviceIndex;

	TUSBEndpoint *m_pEndpointIn;
	TUSBEndpoint *m_pEndpointOut;

	USPiGamePadState   m_State;
	TGamePadStatusHandler *m_pStatusHandler;

	u16 m_usReportDescriptorLength;
	u8 *m_pHIDReportDescriptor;

	TUSBRequest m_URB;
	u8 *m_pReportBuffer;
	u16 m_nReportSize;

	TGamePadType m_GamePadType;

	void (*m_pReportHandler)(struct TUSBGamePadDevice *pThis);
	void (*m_pReportDecoder)(struct TUSBGamePadDevice *pThis);
}
TUSBGamePadDevice;

void USBGamePadDevice (TUSBGamePadDevice *pThis, TUSBFunction *pFunction, TGamePadType nGamePadType);
void _CUSBGamePadDevice (TUSBGamePadDevice *pThis);

boolean USBGamePadStandardDeviceConfigure (TUSBFunction *pUSBFunction);
boolean USBGamePadXbox360DeviceConfigure (TUSBFunction *pUSBFunction);

boolean USBGamePadDeviceStartRequest (TUSBGamePadDevice *pThis);

void USBGamePadDeviceGetReport (TUSBGamePadDevice *pThis);
void USBGamePadDeviceRegisterStatusHandler (TUSBGamePadDevice *pThis, TGamePadStatusHandler *pStatusHandler);

#endif

//
// usbgamepad.c
//
// USPi - An USB driver for Raspberry Pi written in C
// Copyright (C) 2020 flabbergast <flabbergast@drak.xyz>
// Copyright (C) 2014-2018  R. Stange <rsta2@o2online.de>
// Copyright (C) 2014  M. Maccaferri <macca@maccasoft.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <uspi/usbgamepad.h>
#include <uspi/usbgamepadstandard.h>
#include <uspi/usbgamepadxbox360.h>
#include <uspi/usbhostcontroller.h>
#include <uspi/devicenameservice.h>
#include <uspi/assert.h>
#include <uspi/util.h>
#include <uspios.h>

static void USBGamePadDeviceCompletionRoutine (TUSBRequest *pURB, void *pParam, void *pContext);

void USBGamePadDevice (TUSBGamePadDevice *pThis, TUSBFunction *pDevice, TGamePadType nGamePadType)
{
	assert (pThis != 0);

	pThis->m_pEndpointIn = 0;
	pThis->m_pEndpointOut = 0;
	pThis->m_pStatusHandler = 0;
	pThis->m_pHIDReportDescriptor = 0;
	pThis->m_usReportDescriptorLength = 0;
	pThis->m_nReportSize = 0;

	pThis->m_State.naxes = 0;
	for (int i = 0; i < MAX_AXIS; i++) {
		pThis->m_State.axes[i].value = 0;
		pThis->m_State.axes[i].minimum = 0;
		pThis->m_State.axes[i].maximum = 0;
	}

	pThis->m_State.nhats = 0;
	for (int i = 0; i < MAX_HATS; i++)
		pThis->m_State.hats[i] = 0;

	pThis->m_State.nbuttons = 0;
	pThis->m_State.buttons = 0;

	pThis->m_pReportBuffer = malloc (64);
	assert (pThis->m_pReportBuffer != 0);

	assert (nGamePadType < GamePadTypeUnknown);
	pThis->m_GamePadType = nGamePadType;

	USBFunctionCopy (&pThis->m_USBFunction, pDevice);
	switch (nGamePadType)
	{
	case GamePadTypePS3:
		pThis->m_USBFunction.Configure = USBGamePadStandardDeviceConfigure;
		pThis->m_pReportHandler = USBGamePadStandardReportHandler;
		pThis->m_pReportDecoder = USBGamePadStandardDecodeReport;
		break;
	case GamePadTypeXbox360:
		pThis->m_USBFunction.Configure = USBGamePadXbox360DeviceConfigure;
		pThis->m_pReportHandler = USBGamePadXbox360ReportHandler;
		pThis->m_pReportDecoder = USBGamePadXbox360DecodeReport;
		break;
	case GamePadTypeUnknown:
	case GamePadTypeStandard:
	default:
		pThis->m_USBFunction.Configure = USBGamePadStandardDeviceConfigure;
		pThis->m_pReportHandler = USBGamePadStandardReportHandler;
		pThis->m_pReportDecoder = USBGamePadStandardDecodeReport;
		break;
	}
}

void _CUSBGamePadDevice (TUSBGamePadDevice *pThis)
{
	assert (pThis != 0);

	if (pThis->m_pHIDReportDescriptor != 0)
	{
		free (pThis->m_pHIDReportDescriptor);
		pThis->m_pHIDReportDescriptor = 0;
	}

	if (pThis->m_pReportBuffer != 0)
	{
		free (pThis->m_pReportBuffer);
		pThis->m_pReportBuffer = 0;
	}

	if (pThis->m_pEndpointIn != 0)
	{
		_USBEndpoint (pThis->m_pEndpointIn);
		free (pThis->m_pEndpointIn);
		pThis->m_pEndpointIn = 0;
	}

	if (pThis->m_pEndpointOut != 0)
	{
		_USBEndpoint (pThis->m_pEndpointOut);
		free (pThis->m_pEndpointOut);
		pThis->m_pEndpointOut = 0;
	}

	_USBFunction (&pThis->m_USBFunction);
}

void USBGamePadDeviceRegisterStatusHandler (TUSBGamePadDevice *pThis, TGamePadStatusHandler *pStatusHandler)
{
	assert (pThis != 0);
	assert (pStatusHandler != 0);
	pThis->m_pStatusHandler = pStatusHandler;
}

boolean USBGamePadDeviceStartRequest (TUSBGamePadDevice *pThis)
{
	assert (pThis != 0);

	assert (pThis->m_pEndpointIn != 0);
	assert (pThis->m_pReportBuffer != 0);

	USBRequest (&pThis->m_URB, pThis->m_pEndpointIn, pThis->m_pReportBuffer, pThis->m_nReportSize, 0);
	USBRequestSetCompletionRoutine (&pThis->m_URB, USBGamePadDeviceCompletionRoutine, 0, pThis);

	return DWHCIDeviceSubmitAsyncRequest (USBFunctionGetHost (&pThis->m_USBFunction), &pThis->m_URB);
}

void USBGamePadDeviceCompletionRoutine (TUSBRequest *pURB, void *pParam, void *pContext)
{
	TUSBGamePadDevice *pThis = (TUSBGamePadDevice *) pContext;
	assert (pThis != 0);

	assert (pURB != 0);
	assert (&pThis->m_URB == pURB);

	if (   USBRequestGetStatus (pURB) != 0
		&& USBRequestGetResultLength (pURB) > 0)
	{
		//DebugHexdump (pThis->m_pReportBuffer, 16, "report");
		if (pThis->m_pReportHandler != 0)
		{
			(*pThis->m_pReportHandler) (pThis);
		}
	}

	_USBRequest (&pThis->m_URB);

	USBGamePadDeviceStartRequest (pThis);
}

void USBGamePadDeviceGetReport (TUSBGamePadDevice *pThis)
{
	if (DWHCIDeviceControlMessage (USBFunctionGetHost (&pThis->m_USBFunction),
					   USBFunctionGetEndpoint0 (&pThis->m_USBFunction),
					   REQUEST_IN | REQUEST_CLASS | REQUEST_TO_INTERFACE,
					   GET_REPORT, (REPORT_TYPE_INPUT << 8) | 0x00,
					   USBFunctionGetInterfaceNumber (&pThis->m_USBFunction),
					   pThis->m_pReportBuffer, pThis->m_nReportSize) > 0)
	{
		(*pThis->m_pReportDecoder) (pThis);
	}
}


//
// usbgamepadxbox360.c
//
// USPi - An USB driver for Raspberry Pi written in C
// Copyright (C) 2021 flabbergast <flabbergast@drak.xyz>
//
// Mostly ported from Circle: https://github.com/rsta2/circle
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <uspi/usbgamepad.h>
#include <uspi/usbgamepadxbox360.h>
#include <uspi/usbhostcontroller.h>
#include <uspi/devicenameservice.h>
#include <uspi/assert.h>
#include <uspi/util.h>
#include <uspios.h>

unsigned s_nDeviceNumber = 1;

static const char FromUSBPadXbox360[] = "usbxpad360";

static boolean USBGamePadXbox360SetLEDMode (TUSBGamePadDevice *pThis, TGamePadLEDMode Mode);

typedef struct __attribute__((packed))
{
	u16	Header;
#define XBOX360_REPORT_HEADER			0x1400

	u16	Buttons;
#define XBOX360_REPORT_BUTTONS			16

#define XBOX360_REPORT_ANALOG_BUTTONS		2
	u8	AnalogButton[XBOX360_REPORT_ANALOG_BUTTONS];
#define XBOX360_REPORT_ANALOG_BUTTON_MINIMUM	0
#define XBOX360_REPORT_ANALOG_BUTTON_THRESHOLD	128
#define XBOX360_REPORT_ANALOG_BUTTON_MAXIMUM	255

#define XBOX360_REPORT_AXES			4
	s16	Axes[XBOX360_REPORT_AXES];
#define XBOX360_REPORT_AXES_MINIMUM		(-32768)
#define XBOX360_REPORT_AXES_MAXIMUM		32767
} TXbox360Report;

#define XBOX360_REPORT_SIZE	sizeof (TXbox360Report)


void USBGamePadXbox360DecodeReport(TUSBGamePadDevice *pThis)
{
	assert (pThis != 0);

	const TXbox360Report *pReport = (TXbox360Report *) (pThis->m_pReportBuffer);
	assert (pReport != 0);
	// this next fails once for the GPi Case "pretend Xbox360" gamepad
	assert (pReport->Header == XBOX360_REPORT_HEADER);

	static const u32 ButtonMap[] =
	{
		GamePadButtonUp,
		GamePadButtonDown,
		GamePadButtonLeft,
		GamePadButtonRight,
		GamePadButtonStart,
		GamePadButtonBack,
		GamePadButtonL3,
		GamePadButtonR3,
		GamePadButtonLB,
		GamePadButtonRB,
		GamePadButtonXbox,
		0,
		GamePadButtonA,
		GamePadButtonB,
		GamePadButtonX,
		GamePadButtonY
	};

	u32 nButtons = pReport->Buttons;
	pThis->m_State.buttons = 0;
	for (unsigned i = 0; i < XBOX360_REPORT_BUTTONS; i++)
	{
		if (nButtons & 1)
		{
			pThis->m_State.buttons |= ButtonMap[i];
		}

		nButtons >>= 1;
	}

	static const unsigned AxisMap[] =
	{
		GamePadAxisLeftX,
		GamePadAxisLeftY,
		GamePadAxisRightX,
		GamePadAxisRightY,
		GamePadAxisButtonLT,
		GamePadAxisButtonRT
	};

	for (unsigned i = 0; i < XBOX360_REPORT_AXES; i++)
	{
		int nValue = pReport->Axes[i];

		// remap axis value to default range [0, 255]
		nValue = (unsigned) (nValue - XBOX360_REPORT_AXES_MINIMUM) >> 8;

		unsigned nAxis = AxisMap[i];
		if (   nAxis == GamePadAxisLeftY
			|| nAxis == GamePadAxisRightY)	// Y-axes have to be reversed
		{
			nValue = GAMEPAD_AXIS_DEFAULT_MAXIMUM - nValue;
		}

		pThis->m_State.axes[nAxis].value = nValue;
	}

	for (unsigned i = 0; i < XBOX360_REPORT_ANALOG_BUTTONS; i++)
	{
		pThis->m_State.axes[AxisMap[XBOX360_REPORT_AXES+i]].value = pReport->AnalogButton[i];

		if (pReport->AnalogButton[i] >= XBOX360_REPORT_ANALOG_BUTTON_THRESHOLD)
		{
			pThis->m_State.buttons |= GamePadButtonLT << i;
		}
	}
}

static boolean USBGamePadXbox360SetLEDMode (TUSBGamePadDevice *pThis, TGamePadLEDMode Mode)
{
	assert (pThis != 0);

	static const u8 LEDMode[] = {0x00, 0x06, 0x07, 0x08, 0x09};

	if (Mode >= sizeof LEDMode / sizeof LEDMode[0])
	{
		return FALSE;
	}

	static u8 Command[3];
	Command[0] = 0x01;
	Command[1] = 0x03;
	Command[2] = LEDMode[Mode];

	if (DWHCIDeviceTransfer (USBFunctionGetHost (&pThis->m_USBFunction),
				pThis->m_pEndpointOut, Command, 3) < 0)
	{
		LogWrite (FromUSBPadXbox360, LOG_ERROR, "Cannot set LED mode");

		return FALSE;
	}
	return TRUE;
}

boolean USBGamePadDeviceConfigure (TUSBGamePadDevice *pThis)
{
	assert (pThis != 0);

	if (USBFunctionGetNumEndpoints (&pThis->m_USBFunction) <  1)
	{
		USBFunctionConfigurationError (&pThis->m_USBFunction, FromUSBPadXbox360);

		return FALSE;
	}

	TUSBHIDDescriptor *pHIDDesc = (TUSBHIDDescriptor *) USBFunctionGetDescriptor (&pThis->m_USBFunction, DESCRIPTOR_HID);
	if (   pHIDDesc == 0
		|| pHIDDesc->wReportDescriptorLength == 0)
	{
		USBFunctionConfigurationError (&pThis->m_USBFunction, FromUSBPadXbox360);

		return FALSE;
	}

	const TUSBEndpointDescriptor *pEndpointDesc;
	while ((pEndpointDesc = (TUSBEndpointDescriptor *) USBFunctionGetDescriptor (&pThis->m_USBFunction, DESCRIPTOR_ENDPOINT)) != 0)
	{
		if ((pEndpointDesc->bmAttributes & 0x3F) == 0x03)       // Interrupt
		{
			if ((pEndpointDesc->bEndpointAddress & 0x80) == 0x80)   // Input
			{
				if (pThis->m_pEndpointIn != 0)
				{
					USBFunctionConfigurationError (&pThis->m_USBFunction, FromUSBPadXbox360);

					return FALSE;
				}

				pThis->m_pEndpointIn = (TUSBEndpoint *) malloc (sizeof (TUSBEndpoint));
				assert (pThis->m_pEndpointIn != 0);
				USBEndpoint2 (pThis->m_pEndpointIn, USBFunctionGetDevice (&pThis->m_USBFunction), pEndpointDesc);
			}
			else                            // Output
			{
				if (pThis->m_pEndpointOut != 0)
				{
					USBFunctionConfigurationError (&pThis->m_USBFunction, FromUSBPadXbox360);

					return FALSE;
				}

				pThis->m_pEndpointOut = (TUSBEndpoint *) malloc (sizeof (TUSBEndpoint));
				assert (pThis->m_pEndpointOut != 0);
				USBEndpoint2 (pThis->m_pEndpointOut, USBFunctionGetDevice (&pThis->m_USBFunction), pEndpointDesc);
			}
		}
	}

	if (pThis->m_pEndpointIn == 0)
	{
		USBFunctionConfigurationError (&pThis->m_USBFunction, FromUSBPadXbox360);

		return FALSE;
	}

	// block1: this is done much later in the GamePadStandardConfigure
	if (!USBFunctionConfigure (&pThis->m_USBFunction))
	{
		LogWrite (FromUSBPadXbox360, LOG_ERROR, "Cannot set interface");

		return FALSE;
	}
	// end_block1

	// block2: this is probably for HID class/keyboards; shouldn't apply to gamepads...
	if (   USBFunctionGetInterfaceClass (&pThis->m_USBFunction)    == 3  // HID class
		&& USBFunctionGetInterfaceSubClass (&pThis->m_USBFunction) == 1) // boot class
	{
		if (DWHCIDeviceControlMessage (USBFunctionGetHost (&pThis->m_USBFunction),
									   USBFunctionGetEndpoint0 (&pThis->m_USBFunction),
									   REQUEST_OUT | REQUEST_CLASS | REQUEST_TO_INTERFACE,
									   SET_PROTOCOL,
									   USBFunctionGetInterfaceProtocol (&pThis->m_USBFunction) == 2
											? REPORT_PROTOCOL : BOOT_PROTOCOL,
									   USBFunctionGetInterfaceNumber (&pThis->m_USBFunction), 0, 0) < 0)
		{
			LogWrite (FromUSBPadXbox360, LOG_ERROR, "Cannot set protocol (but should not get here at all)");

			return FALSE;
		}
	}
	// end_block2
	// block3: Circle::CUSBHIDDevice::Configure also allocates report buffer here
	//   but here it's done in the "constructor" function
	// end_block3
	// Circle::CUSBHIDDevice::Configure ends here

	pThis->m_nDeviceIndex = s_nDeviceNumber++;

	TString DeviceName;
	String (&DeviceName);
	StringFormat (&DeviceName, "upad%u", pThis->m_nDeviceIndex);
	DeviceNameServiceAddDevice (DeviceNameServiceGet (), StringGet (&DeviceName), pThis, FALSE);

	_String (&DeviceName);

	return TRUE;
	// Circle::CUSBGamePadDevice::Configure ends here
}

boolean USBGamePadXbox360DeviceConfigure (TUSBFunction *pUSBFunction)
{
	TUSBGamePadDevice *pThis = (TUSBGamePadDevice *) pUSBFunction;
	assert (pThis != 0);

	if (!USBGamePadDeviceConfigure (pThis))
	{
		LogWrite (FromUSBPadXbox360, LOG_ERROR, "Cannot configure gamepad device");
		
		return FALSE;	
	}

	pThis->m_nReportSize = XBOX360_REPORT_SIZE;

	pThis->m_State.nbuttons = XBOX360_REPORT_BUTTONS+XBOX360_REPORT_ANALOG_BUTTONS;
	pThis->m_State.naxes = XBOX360_REPORT_AXES+XBOX360_REPORT_ANALOG_BUTTONS;
	for (unsigned i = 0; i < XBOX360_REPORT_AXES; i++)
	{
		pThis->m_State.axes[i].minimum = GAMEPAD_AXIS_DEFAULT_MINIMUM;
		pThis->m_State.axes[i].maximum = GAMEPAD_AXIS_DEFAULT_MAXIMUM;
	}
	for (unsigned i = 0; i < XBOX360_REPORT_ANALOG_BUTTONS; i++)
	{
		pThis->m_State.axes[XBOX360_REPORT_AXES+i].minimum = XBOX360_REPORT_ANALOG_BUTTON_MINIMUM;
		pThis->m_State.axes[XBOX360_REPORT_AXES+i].maximum = XBOX360_REPORT_ANALOG_BUTTON_MAXIMUM;
	}
	pThis->m_State.nhats = 0;

	if (!USBGamePadXbox360SetLEDMode (pThis, (TGamePadLEDMode) pThis->m_nDeviceIndex))
	{
		return FALSE;
	}

	return USBGamePadDeviceStartRequest (pThis);
}

void USBGamePadXbox360ReportHandler (TUSBGamePadDevice *pThis)
{
	assert (pThis != 0);

	if ( pThis->m_pReportBuffer != 0
		&& pThis->m_nReportSize == XBOX360_REPORT_SIZE
		&& pThis->m_pReportBuffer[0] == (XBOX360_REPORT_HEADER & 0xFF)
		&& pThis->m_pReportBuffer[1] == (XBOX360_REPORT_HEADER >> 8)
		&& pThis->m_pStatusHandler != 0)
	{
		USBGamePadXbox360DecodeReport (pThis);

		(*pThis->m_pStatusHandler) (pThis->m_nDeviceIndex - 1, &pThis->m_State);
	}
}

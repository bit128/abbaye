# l'Abbaye des Morts (and bare metal templates) for GPi Case

The point was to have a [SDL2] basis for running bare metal applications on
[Retroflag GPi Case] (with a [Raspberry Pi Zero]). l'Abbaye des Morts
is a freeware game (by LocoMalito & Gryzor87 in 2010), it's here
to check that everything works.

Almost all of this has been successfully done in 2014 by Marco
Maccaferri: [code on github][abbaye] - Abbaye running bare metal on a Raspberry
Pi Zero, video through HDMI, controlled via keyboard.

By now there are two really excellent "base" libraries for writing bare
metal applications on a Raspberry Pi:  R. Stange's [circle] library (C++)
and [uspi] library (plain C). Circle is more updated and supports quite
a lot more USB devices, so for starting anew, one should really go for
[circle]. However Maccaferri's code already incorporates the [SDL2] library...

So, what's here? Somewhat updated Maccaferri's code, adjusted specifically to
run on a GPi Case.
What are the adjustments, i.e. can this run on a Pi Zero outside of a GPi Case?
In principle, yes. It expects an "Xbox360" gamepad (nb. the 8BitDo controllers
pretend to be those). It expects the screen to be a DPI screen - but this is
adjustable in config.txt and does not need recompiling. It generates sound
on Pi pins 18 and 19 (changing this requires recompiling; you can build a simple
circuit to get sound into headphones: [writeup here][blog:pwmsound]). It has an
extra startup delay (see GPi Case quirks below for the reason).

NB. Raspberries above 1 are not really supported.

## Caveats

While this version of Abbaye uses SDL2, there are a couple of points to be
aware of, especially if the code should be used for something else.

It is based on [linux version][abbaye-linux] of the game by nevat.

SDL2 is on version 2.0.3 (which is pretty old by now). Also, the version here
also only has a software renderer for the Pi, which makes drawing sprites
"the SDL way" (i.e. using SDL_RenderCopy and Textures) too slow - even for a
simple game like Abbaye.

In the code here, this is worked around by effectively using a custom function
'blit', which in turn calls kernel's 'fb_blit' directly, and draws to the window
surface directly. However this seems to rely on some SDL2 library internals, and
it breaks when SDL2 is updated.  (See one of the other branches where SDL2 is
updated, and Abbaye no longer works as expected.)

(So, the way forward is to actually migrate SDL2's hardware renderer for the
Raspberries so that it works on bare metal as well...)

## Compiling

See also the original README file.

Basically you'll need an arm toolchain (arm-none-eabi-gcc) and make. (Linux
recommended).

Possibly tweak the Makefiles. (E.g. you can create `kernel/config.mk` with
`CFLAGS += -DHAVE_UART_LOGGER` to have see some log output on uart pins
from the templates.)

Run `make` (which will attempt to compile all the libraries
and then the executables).

The "executables" are called `kernel.img`, you'll get one for each template,
and one for abbaye.


## Running

Copy `kernel.img` along with the startup files onto the single FAT32 formatted
partition of a micro SD card.

You can get the minimal startup files by running `make` in the `boot`
subdirectory. The files you need from `boot` are `start.elf`,
`fixup.dat`, `bootcode.bin`, `config.txt`, and `control.txt` (this is used by
SDL2 for correct mapping of the GPi Case gamepad).

If you Zero isn't inside a GPi case, use `config.txt.not_gpicase` in place of
`config.txt`. You may also need to change `control.txt` if the reactions
to your gamepad aren't what you would expect (only for `abbaye`).


## Startup time

This was my main motivation: to minimise the startup time.

From the powerup, Pi Zero initialisation takes about ~3.5 seconds: the MCU reads
the Broadcom startup binaries, runs them, they read `config.txt` and adjust the
hardware config, then read `kernel.img` and start executing it.
(There is nothing that can be done about this.)

The bare metal code here starts up pretty quickly; things like screen init and
audio init take almost no time. What takes ~0.5-1 second is USB initialisation.
Note that this is really only done once (and quickly) on startup, and some USB
devices may not be ready (e.g. the GPi Case controller, or my TMK-firmware based
keyboard). (NB. This is where [circle] does much better in terms of compatibility,
but a couple of seconds of delay.)

In any case, the GPi Case "gamepad controller" is really unpleasant; it needs
~1.5 - 2 second artificial delay before trying to enumerate it, otherwise it
doesn't get reliably detected. The templates show a "waiting" screen during
this time.

All in all, the app should be running in about ~5 seconds after powerup.


## GPi Case idiosyncracies

[Retroflag GPi Case] basically adds some hardware to a Raspberry Pi Zero. Namely:

- A 320x240 LCD screen, hooked to the DPI interface of the RPi. [This reddit
  post](https://www.reddit.com/r/retroflag_gpi/comments/cenjwv/further_tinkering_with_the_overlay_files/)
  explains it pretty well.
- An audio amplifier (apa2603a) hooked to pins 18 and 19 (which can be selected
  for PWM output of the Pi). Note that the audio jack on RPi 1 also uses the
  same internal PWM peripheral to generate sound/audio, but it is hooked to
  different pins.
- A GD32 MCU (which is a sort of a "clone" of the ubiquitous STM32F103 MCUs)
  which pretends to be a USB Xbox360 gamepad to the RPi, and services the
  buttons on the case.
- The case further has a volume wheel, a brightness wheel, and an audio jack -
  these are completely independent of the RPi, and go directly to the relevant
  hardware.

The LCD screen is relatively easy to deal with - it doesn't need any special
handing in the bare metal code. Just use `config.txt` to have the Broadcom
startup blob set up the correct pin modes for the DPI pins, and the GPU does
the rest automatically.
(Note that the overlay files that one can get from Retroflag or [some other
places][overlays] did not work for me - they seem to have been ignored.
But one can set up the correct pin modes using the `gpio=` settings in
`config.txt`, so they are actually not needed.)

Setting up the PWM/audio output is also relatively easy, since again it only
requires setting the correct pin modes. Again, I do this in the `config.txt`
using `gpio=` settings, because the overlay which was supposed to do this
was ignored. (I still did not figure out the reason why...)

Now actually getting the audio from the speaker/jack on the GPi Case is
interesting - the GD32 controller can disable the onboard amplifier (there are
two lines that are controlled: shutdown and mute); and the default state is
disabled. The GD32 firmware seems to only enable audio after a successful USB
enumeration as a gamepad.

The GD32 seems to wait for about ~5 seconds after powerup until it
allows itself to be enumerated.... and sometimes doesn't cooperate
after a reset....

Finally, there is the "safe shutdown" mechanism. I haven't mapped the
hardware for this, but the way it works is that _if_ the "safe shutdown" slider
in the battery compartment is "on", _and_ one writes 1 to pin 27, _then_
reading 0 on pin 26 means that the power slider was moved to the "off" position,
but the power remains on. (So pin 26 should be an input with an internal pullup
enabled in this case.) Not sure what needs to be done now in the app to turn
the power off...

_If_ the "safe shutdown" slider in the battery compartment is "off"; _or_ when
the slider is "on", but 1 is not written to pin 27, then the main power slider
simply turns the power off when moved to the "off" position. (Am not 100% sure
that it also cuts the power to the GD32, or whether the GD32 firmware needs to
act and put itself to sleep...)

Well, wouldn't it be nice if we could tweak the GD32 firmware ...

### GD32 firmware observations

#### D-pad modes

The GD32 firmware also takes care of setting the "hat mode" and "axis mode" for
D-pad.  In either mode, D-pad presses are sent as different events.

Switching between the two works as follows: Hold START+Left (or SELECT+Left on older
versions of the Case) for 5 seconds and it switches to "axis mode"; you get a visual
confirmation of this happening by the power LED briefly flashing purple.
Hold START+Up (or SELECT+Up on older Cases) for 5 seconds for "hat mode"; again the
power LED briefly flashes purple.

The 'control.txt' mapping file that's supplied here assumes "hat mode" - so if
buttons work but D-pad does not, your Case is in the "axis mode".

#### Firmware update

The GD32 apparently has a bootloader which allows for a firmware update without
having to solder some connections to the board. To get to it, remove the cart,
connect a micro USB cable to the connector in the battery cover opening.
Hold START and SELECT, and power up the board. The power LED should be blinking
purple/red, and the case should enumerate as a mass storage device with no
partitions, FAT32 formatted. There are two 0-size files, 'PUT FIRMWARE HERE',
and (on my case) 'Ver.1.07'. Presumably dropping a file with firmware there
would flash it to GD32. Not sure about the format, or any integrity checks
that may be done.

#### Debugging header

There are some pads on the PCB which link to SWD pins of the GD32. On the "left
side" of the PCB, near the GD32 and very close to the LCD bracket, there is
a row of 7 pads, probably 0.5mm spaced (just like the ribbon cable). They are,
from the top (from the plastic bracket): BOOT0, NRST, GND, PA13 (SWDIO), PA14
(SWCLK), VDD, VDD.


## Some comments on the structure of the code

Now [uspi] (and also [circle]) is very well organised in that it separates the
uspi library itself (which deals with USB), and other "env" code which
takes care of startup tasks, sets up register aliases, and gives convenient
access to peripherals (which are sometimes somewhat painful to deal with
directly): uart, framebuffer, audio, sd card.

In this code, all of the above gets compiled into one "kernel" library; the
uspi/env code is intertwined. Note that the uspi library code itself has been
updated here to 2.0 from [uspi]. Also note that [uspi] currently does not
have an Xbox360 gamepad driver; the one here was ported from [circle].

Going further, there are two "templates" here; one uses just the kernel functions,
the other uses full [SDL2] setup; that takes up a lot more space.




[Retroflag GPi Case]: http://retroflag.com/GPi-CASE.html
[Raspberry Pi Zero]: https://www.raspberrypi.org/products/raspberry-pi-zero/
[abbaye]: https://github.com/maccasoft/raspberry-pi
[uspi]: https://github.com/rsta2/uspi
[circle]: https://github.com/rsta2/circle
[welch]: https://github.com/dwelch67/raspberrypi-zero
[SDL2]: https://libsdl.org/
[overlays]: https://github.com/tsoliman/Retroflag-GPi-case
[blog:pwmsound]: https://shallowsky.com/blog/hardware/pi-zero-audio.html
[abbaye-linux]: https://github.com/nevat/abbayedesmorts-gpl

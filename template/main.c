/*
 * Copyright (c) 2021 flabbergast <flabbergast@drak.xyz>
 * Copyright (c) 2014 Marco Maccaferri and Others
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "kernel.h"
#include "wiring.h"

#ifdef HAVE_USPI
#include "uspi.h"
#include "uspios.h"
#endif // HAVE_USPI

#if BYTES_PER_PIXEL == 2

#define BORDER_COLOR        RGB(26, 5, 10)
#define BACKGROUND_COLOR    RGB(12, 0, 4)

#elif BYTES_PER_PIXEL == 4

#define BORDER_COLOR        RGB(213, 41, 82)
#define BACKGROUND_COLOR    RGB(98, 0, 32)

#endif

#if defined(__cplusplus)
extern "C" {
#endif

__attribute__ ((interrupt ("IRQ"))) void interrupt_irq() {
#ifdef HAVE_USPI
    USPiInterruptHandler ();
#endif // HAVE_USPI
}

#if defined(__cplusplus)
}
#endif

static void audio_init(void)
{
    // pins 18 and 19 to ALT5
    GPIO->gpfsel[1] = (GPIO->gpfsel[1] & ~GPIO_FSEL8_CLR) | GPIO_FSEL8_ALT5;
    GPIO->gpfsel[1] = (GPIO->gpfsel[1] & ~GPIO_FSEL9_CLR) | GPIO_FSEL9_ALT5;
    MsDelay(2);

    CLK->PWMCTL = CM_PASSWORD | (1 << 5); // stop clock

    int idiv = 2; // raspbian has idiv set as 16384
    CLK->PWMDIV = CM_PASSWORD | (idiv<<12);

    CLK->PWMCTL = CM_PASSWORD | 16 | 1; // enable + oscillator
                                        // raspbian has this as plla
    MsDelay(2);

    // disable PWM
    PWM->ctl = 0;

    MsDelay(2);

    PWM->rng1 = 0x400;
    PWM->rng2 = 0x400;

    PWM->ctl =
          PWM_USEF1 |
//          BCM2835_PWM1_REPEATFF |
          PWM_PWEN1 |
          PWM_USEF2 |
//          BCM2835_PWM0_REPEATFF |  */
          PWM_PWEN2 | PWM_CLRF1;

    MsDelay(2);
}

#ifdef HAVE_USPI
static void GamePadStatusHandler (unsigned int nDeviceIndex, const USPiGamePadState *pState)
{
    char str[256];
    sprintf(str, "GamePad %u: Buttons 0x%X", nDeviceIndex + 1, pState->buttons);

    if (pState->naxes > 0) {
        strcat(str, " Axes");
        for (unsigned i = 0; i < pState->naxes; i++) {
            sprintf(str+strlen(str), " %d", pState->axes[i].value);
        }
    }

    if (pState->nhats > 0) {
        strcat(str, " hats");
        for (unsigned i = 0; i < pState->nhats; i++) {
            sprintf(str+strlen(str), " %d", pState->hats[i]);
        }
    }

    LogWrite ("main:upad", LOG_NOTICE, str);
}
#endif // HAVE_USPI

void main() {
    int x, y;
    struct timer_wait tw;
    int led_status = LOW;
    const int led_pin = 47;
    signed int c;

#ifdef HAVE_UART_LOGGER
    uart_logger_init();
#endif // HAVE_UART_LOGGER
    LogWrite("main", LOG_NOTICE, "RasPi bare metal template starting....");

    // playing with 'safe shutdown' on GPi Case
    //GPIO->gpfsel[2] = (GPIO->gpfsel[2] & ~GPIO_FSEL7_CLR) | GPIO_FSEL7_OUT;
    //GPIO->gpset[0] = (1<<27);
    //pinMode(27, OUTPUT); digitalWrite(27, HIGH);
    //pinMode(26, INPUT_PULLUP);

    // Default screen resolution (set in config.txt or auto-detected)
    //fb_init(0, 0);

    // Sets a specific screen resolution
    fb_init(320, 240);

    fb_fill_rectangle(0, 0, fb_width - 1, fb_height - 1, BORDER_COLOR);

    initscr(40, 25);
    cur_fore = WHITE;
    cur_back = BACKGROUND_COLOR;
    clear();

    mvaddstr(1, 9, "**** RASPBERRY-PI ****");
    mvaddstr(3, 7, "BARE-METAL SYSTEM TEMPLATE\r\n");

    if (mount("sd:") != 0) {
        addstrf("\r\nSD CARD NOT MOUNTED (%s)\r\n", strerror(errno));
    }

    addstr("\r\nWAITING FOR THE GPI CASE GAMEPAD...\r\n");
    MsDelay(1300); // waiting for the GPi Case "pretend Xbox360 gamepad" to wake up and smell the coffee

    addstr("INITIALISING USB...\r\n");
    usb_init();
    if (keyboard_init() != 0) {
        addstr(" NO KEYBOARD DETECTED\r\n");
    }

#ifdef HAVE_USPI
    int n_gamepads = USPiGamePadAvailable();
    if (n_gamepads < 1) {
        LogWrite("main", LOG_ERROR, "no gamepad found");
    } else {
        for(unsigned n = 0; n < (unsigned) n_gamepads; n++) {
            TUSPiDeviceInformation Info;
            if (!USPiDeviceGetInformation (GAMEPAD_CLASS, n, &Info)) {
                LogWrite ("main", LOG_ERROR, "Cannot get device information for gamepad %d",n);
            }
            LogWrite ("main", LOG_NOTICE, "GamePad %u: Vendor 0x%X Product 0x%X Version 0x%X",
                  n+1, (unsigned) Info.idVendor, (unsigned) Info.idProduct, (unsigned) Info.bcdDevice);

            LogWrite ("main", LOG_NOTICE, "GamePad %u: Manufacturer \"%s\" Product \"%s\"",
                  n+1, Info.pManufacturer, Info.pProduct);

            const USPiGamePadState *pState = USPiGamePadGetStatus (n);

            LogWrite ("main", LOG_NOTICE, "GamePad %u: %d Buttons %d Hats",
                  n+1, pState->nbuttons, pState->nhats);

            for (int i = 0; i < pState->naxes; i++) {
                LogWrite ("main", LOG_NOTICE, "GamePad %u: Axis %d: Minimum %d Maximum %d",
                      n+1, i+1, pState->axes[i].minimum, pState->axes[i].maximum);
            }
        }
        // this can flood the logs, but can be used to check whether a controller works
        USPiGamePadRegisterStatusHandler(GamePadStatusHandler);
    }
    addstrf(" %d GAMEPAD(S) FOUND\r\n", n_gamepads);
#endif // HAVE_USPI

    pinMode(led_pin, OUTPUT);
    register_timer(&tw, 250000);

    // this shows the value of the GPIO_GPFSEL1 register, which
    //   has the mode of pins 18 and 19 - audio/pwm is played on these
    addstrf("GPFSEL1 = 0x%08x\r\n", *(volatile unsigned int *)(0x20200004) );

    audio_init();
    int i=0;
    long status;

    #define ERRORMASK (PWM_GAPO2 | PWM_GAPO1 | PWM_RERR1 | PWM_WERR1)

    addstr("\r\nREADY\r\n");

    // testing file IO
    FILE *f = fopen("control.txt", "r");
    if(f == NULL) {
        LogWrite("main", LOG_ERROR, "Could not open control.txt file");
    } else {
        char chunk[128];

        while(fgets(chunk, sizeof(chunk), f) != NULL) {
            LogWrite("main", LOG_DEBUG, "chunk size: %zd; got: %s", strlen(chunk), chunk);
        }
        fclose(f);
    }

    while(1) {
        if ((c = getch()) != -1) {
            switch(c) {
                case KEY_UP:
                    getyx(y, x);
                    move(y - 1, x);
                    break;
                case KEY_DOWN:
                    getyx(y, x);
                    move(y + 1, x);
                    break;
                case KEY_LEFT:
                    getyx(y, x);
                    x--;
                    if (x < 0) {
                        x = 39;
                        y--;
                    }
                    move(y, x);
                    break;
                case KEY_RIGHT:
                    getyx(y, x);
                    x++;
                    if (x >= 40) {
                        x = 0;
                        y++;
                    }
                    move(y, x);
                    break;
                case KEY_HOME:
                    getyx(y, x);
                    move(y, 0);
                    break;
                case KEY_PGUP:
                    move(0, 0);
                    break;
                case '\r':
                    addch(c);
                    addch('\n');
                    break;
                default:
                    if (c < 0x7F) {
                        addch(c);
                    }
                    break;
            }
        }
        status = PWM->sta;
        if (!(status & PWM_FULL1)) {
            PWM->fif1 = 20*(i & 0x1f) ;
            i++;
        }
        if ((status & ERRORMASK)) {
            //addstrf("|x%x", status);
            // getting a bunch of "gap occured"
            PWM->sta = ERRORMASK;
        }

        if (compare_timer(&tw)) {
            led_status = led_status == LOW ? HIGH : LOW;
            digitalWrite(led_pin, led_status);
            toggle_cursor();
        }

#ifdef HAVE_UART_LOGGER
        if( (c=uart_getc()) != -1 ) {
            uart_putc( (unsigned char)c );
        }
#endif // HAVE_UART_LOGGER

        refresh();
    }
}

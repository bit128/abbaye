/* Abbaye des Morts */
/* Version 2.0 */

/* (c) 2010 - Locomalito & Gryzor87 */
/* 2013 - David "Nevat" Lara */

/* GPL v3 license */

#include <stdio.h>
#include <stdlib.h>
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_mixer.h"

#include "kernel.h"

#ifdef GPI_CASE
#include "fb.h"
#include "console.h"
#include "uspi.h"
#include "uspios.h"
#endif // GPI_CASE

extern void startscreen(SDL_Window *screen,unsigned int *state,unsigned int *grapset,unsigned int *fullscreen);
extern void history(SDL_Window *screen,unsigned int *state,unsigned int *grapset,unsigned int *fullscreen);
extern void game(SDL_Window *screen,unsigned int *state,unsigned int *grapset,unsigned int *fullscreen);
extern void gameover (SDL_Window *screen,unsigned int *state);
extern void ending (SDL_Window *screen,unsigned int *state);

#if defined(__cplusplus)
extern "C" {
#endif

__attribute__ ((interrupt ("IRQ"))) void interrupt_irq() {
    SDL_Interrupt_Handler();
}

#if defined(__cplusplus)
}
#endif

void main () {

#ifdef GPI_CASE
    // GPi Case splash while waiting for the gamepad

#define BORDER_COLOR        RGB(213, 41, 82)
#define BACKGROUND_COLOR    RGB(98, 0, 32)

    fb_init(320, 240);

    fb_fill_rectangle(0, 0, fb_width - 1, fb_height - 1, BORDER_COLOR);

    initscr(40, 25);
    cur_fore = WHITE;
    cur_back = BACKGROUND_COLOR;
    clear();

    mvaddstr(1, 5, "**** GPi CASE BARE METAL ****");
    mvaddstr(3, 2, "Compiled on "__DATE__);
    mvaddstr(4, 2, "Code by: flabbergast,");
    mvaddstr(5, 11, "M.Maccaferri, R.Stange,");
    mvaddstr(6, 11, "and others");
    mvaddstr(7, 2, "l'Abbaye des Morts game by");
    mvaddstr(8, 4, "LOCOMALITO & GRYZOR87");
    mvaddstr(9, 4, "SDL version by NEVAT");
    mvaddstr(11, 2, "WAITING FOR GAMEPAD...\r\n");
    MsDelay(2000);
    mvaddstr(13, 2, "INITIALISING...\r\n");
#endif // GPI_CASE

	unsigned int state = 0; /* 0-intro,1-history,2-game */
	unsigned int grapset = 1; /* 0-8bits, 1-16bits */
	unsigned int fullscreen = 0; /* 0-Windowed,1-Fullscreen */

	mount("sd:");

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_EVENTS | SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER);

	/* Creating window */
	SDL_Window *screen = SDL_CreateWindow("Abbaye des Morts v2.0",0,0,256,192,SDL_WINDOW_FULLSCREEN);

	/* Init audio */
	Mix_OpenAudio (22050,MIX_DEFAULT_FORMAT,2,4096);
	Mix_AllocateChannels(5);

	/* Init game controllers */
    SDL_GameControllerAddMappingsFromFile("control.txt");
    for (int i = 0; i < SDL_NumJoysticks(); i++) {
        if (SDL_IsGameController(i)) {
            SDL_GameControllerOpen(i);
        }
    }

	while (1) {
		switch (state) {
			case 0: startscreen(screen,&state,&grapset,&fullscreen);
							break;
			case 1: history(screen,&state,&grapset,&fullscreen);
							break;
			case 2: game(screen,&state,&grapset,&fullscreen);
							break;
			case 3: gameover(screen,&state);
							break;
			case 4: ending(screen,&state);
							break;
		}
	}

}
